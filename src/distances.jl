#TODO: Refactor to use https://github.com/JuliaStats/Distances.jl ?
#TODO: Add more distance functions? 

"""
    euclidean(pt::Tuple, idxs::AbstractVector)

Euclidean distance between a point `pt` and an array of points `idxs`.
"""
euclidean(pt::Tuple, idxs::AbstractVector) = [sqrt(sum((pt .- idx) .^ 2)) for idx in idxs]
