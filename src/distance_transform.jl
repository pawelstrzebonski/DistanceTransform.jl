#TODO: Implement multiple distance functions

function distance_transform_naive(img::BitArray)
    # Identify all of the border-points
    e = edge_transform(img)
    # Split into border 1's and border 0's
    z = vec(iszero.(img) .& e)
    o = vec(isone.(img) .& e)
    # Create lists of all the indices of the relevant border points
    idxs = vec([Tuple(idx) for idx in eachindex(IndexCartesian(), img)])
    zidxs = idxs[z]
    oidxs = idxs[o]
    # For each point, find the minimal distance to a point of the opposite value
    dm = [
        iszero(img[idx]) ? minimum(euclidean(pt, oidxs)) :
            -minimum(euclidean(pt, zidxs)) for (idx, pt) in enumerate(idxs)
    ]
    # Reshape into original shape
    reshape(dm, size(img))
end

#TODO: Not certain if there's any merit to this function/algorithm. Remove?
function distance_transform_distancetable(img::BitArray)
    # Identify all of the border-points
    e = edge_transform(img)
    # Split into border 1's and border 0's
    zimg = iszero.(img) .& e
    oimg = isone.(img) .& e
    # Create lists of indices for the border points
    s = size(img)
    CIs = CartesianIndices(s) .- CartesianIndex(Tuple(ones(Int, ndims(img))))
    CIz = CIs[zimg]
    CIo = CIs[oimg]
    # Calculate look-up table for distances
    dmap = reshape(euclidean(s, vec(Tuple.(CartesianIndices(s .* 2 .- 1)))), s .* 2 .- 1)
    # Offset value for calculating current index in original array
    Cmx = CartesianIndex(s) + CartesianIndex(Tuple(ones(Int, ndims(img))))
    # Iterate through all points finding the minimal distance to a point of different values
    [
        iszero(img[Cmx-idx]) ? minimum(dmap[[idx].+CIo]) : -minimum(dmap[[idx].+CIz])
        for idx in Iterators.reverse(eachindex(IndexCartesian(), img))
    ]
end

"""
    distance_transform(img::BitArray)

Calculate the (signed) distance transform for a binary array.
"""
distance_transform = distance_transform_naive
