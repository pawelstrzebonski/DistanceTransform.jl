"""
    edge_transform(img::BitArray)

Takes a binary image and returns a binary array where all elements that border
an edge are `true`.
"""
function edge_transform(img::BitArray)
    rng =
        CartesianIndices(Tuple(fill(3, ndims(img)))) .-
        CartesianIndex(Tuple(ones(Int, ndims(img))))
    edge = trues(size(img))
    edge[(x -> 2:(x-1)).(size(img))...] .= [
        any(img[rng.+CartesianIndex(idx)] .!== img[idx])
        for idx in eachindex(view(img, (x -> 2:(x-1)).(size(img))...))
    ]
    edge
end
