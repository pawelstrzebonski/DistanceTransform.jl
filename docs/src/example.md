# Usage and Examples

## About

This package supports arbitrary dimensional arrays/image that are
`BitArray`s. As of writing, only signed Euclidean distance is implemented,
whereby a positive value indicates the the distance to the nearest `1`
element for a `0` element and a negative value the distance to the
nearest `0` element for a `1` element.

## 1D Images

We create a 1D "image", calculate the signed distance, and plot both
the image and distance transform:

```@example 1d
import DistanceTransform
import Plots: plot
import Plots: savefig # hide

img = falses(50)
img[10:20] .= true
img[30:45] .= true
dt = DistanceTransform.distance_transform(img)
plot(
	plot(img),
	plot(dt),
	legend = false,
)
savefig("example1d.svg") # hide
nothing # hide
```

![1D Distance Transform](example1d.svg)

## 2D Images

We create a 2D image, calculate the signed distance, and plot both
the image and distance transform:

```@example 2d
import DistanceTransform
import Plots: plot, heatmap
import Plots: savefig # hide

img = falses(50, 50)
img[10:40, 10:40] .= true
img[20:30, 20:30] .= false
dt = DistanceTransform.distance_transform(img)
plot(
	heatmap(img),
	heatmap(dt),
)
savefig("example2d.png") # hide
nothing # hide
```

![2D Distance Transform](example2d.png)
