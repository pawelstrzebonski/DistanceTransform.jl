# edge_transform.jl

## Description

Function for identifying all border/edge points.

## Functions

```@autodocs
Modules = [DistanceTransform]
Pages   = ["edge_transform.jl"]
```
