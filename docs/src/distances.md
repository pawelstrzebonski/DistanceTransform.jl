# distances.jl

## Description

Calculation of the distance metrics.

## Functions

```@autodocs
Modules = [DistanceTransform]
Pages   = ["distances.jl"]
```
