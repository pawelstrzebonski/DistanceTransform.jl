# DistanceTransform.jl Documentation

## About

`DistanceTransform` is a Julia package for calculating the distance
transforms of arbitrary dimensional images/arrays.

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/DistanceTransform.jl
```

## Features

* Arbitrary dimensional input support
* Implements signed Euclidean distance
