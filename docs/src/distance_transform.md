# distance_transform.jl

## Description

Distance transform calculation.

## Functions

```@autodocs
Modules = [DistanceTransform]
Pages   = ["distance_transform.jl"]
```
