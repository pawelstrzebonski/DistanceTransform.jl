using Documenter
import DistanceTransform

makedocs(
    sitename = "DistanceTransform.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "DistanceTransform.jl"),
    pages = [
        "Home" => "index.md",
        "Example Usage" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "distances.jl" => "distances.md",
            "distance_transform.jl" => "distance_transform.md",
            "edge_transform.jl" => "edge_transform.md",
        ],
        "test/" => [
            "distances.jl" => "distances_test.md",
            "distance_transform.jl" => "distance_transform_test.md",
            "edge_transform.jl" => "edge_transform_test.md",
        ],
    ],
)
