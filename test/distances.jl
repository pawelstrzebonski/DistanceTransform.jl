@testset "distances.jl" begin
    pt = (2,)
    idxs = [(i + 1,) for i = 1:10]
    @test DistanceTransform.euclidean(pt, idxs) == 0:9
    pt = (1, 2)
    idxs = [(1, i + 1) for i = 1:10]
    @test DistanceTransform.euclidean(pt, idxs) == 0:9
    pt = (1, 2, 3)
    idxs = [(i, i + 1, i + 2) for i = 1:10]
    @test isapprox(DistanceTransform.euclidean(pt, idxs), (0:9) .* sqrt(3))
end
