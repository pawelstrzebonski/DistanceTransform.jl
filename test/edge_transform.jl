@testset "edge_transform.jl" begin
    img = falses(10)
    imge = falses(size(img))
    imge[1] = imge[end] = true
    @test DistanceTransform.edge_transform(img) == imge
    img = falses(10, 23)
    imge = falses(size(img))
    imge[:, 1] .= true
    imge[:, end] .= true
    imge[1, :] .= true
    imge[end, :] .= true
    @test DistanceTransform.edge_transform(img) == imge
    img = trues(10, 23)
    @test DistanceTransform.edge_transform(img) == imge
    img = falses(10, 23)
    img[5, 5] = true
    imge[4:6, 4:6] .= true
    @test DistanceTransform.edge_transform(img) == imge
end
