@testset "edge_transform.jl" begin
    #TODO: Add tests for actual correct values
    img = falses(10)
    img[4:7] .= true
    @test (DistanceTransform.distance_transform(img); true)
    img = falses(10, 11)
    img[4:7, 4:7] .= true
    @test (DistanceTransform.distance_transform(img); true)
    img = falses(10, 11, 12)
    img[4:7, 4:7, 4:7] .= true
    @test (DistanceTransform.distance_transform(img); true)
    @test DistanceTransform.distance_transform(img) ==
          DistanceTransform.distance_transform_naive(img)
    @test DistanceTransform.distance_transform_naive(img) ==
          DistanceTransform.distance_transform_distancetable(img)
end
