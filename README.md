# DistanceTransform

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pawelstrzebonski.gitlab.io/DistanceTransform.jl)
[![Build Status](https://gitlab.com/pawelstrzebonski/DistanceTransform.jl/badges/master/pipeline.svg)](https://gitlab.com/pawelstrzebonski/DistanceTransform.jl/pipelines)
[![Coverage](https://gitlab.com/pawelstrzebonski/DistanceTransform.jl/badges/master/coverage.svg)](https://gitlab.com/pawelstrzebonski/DistanceTransform.jl/commits/master)

## About

`DistanceTransform.jl` is a Julia package for calculating the distance
transforms of arbitrary dimensional images/arrays.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/DistanceTransform.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for DistanceTransform.jl](https://pawelstrzebonski.gitlab.io/DistanceTransform.jl/).
